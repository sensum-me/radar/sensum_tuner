#!/usr/bin/env python3

import rospy
import socket
from struct import pack
from dynamic_reconfigure.server import Server
from sensum_tuner.cfg import one_radarConfig
from sensum_tuner.cfg import two_radarsConfig
from sensum_tuner.cfg import two_clouds_protocolConfig

#%%

def client(ip_port, output, sock):
    try:
        HOST, PORT = ip_port.split(':')
    except:
        rospy.loginfo("If you want to configure only 1 radar set the /\
                       field to this state \':\'")
    try:
        # print(HOST, PORT)
        if HOST != '' and PORT != '':
            sock.sendto(output, (HOST, int(PORT)))
    except:
        rospy.loginfo("Radar is't conected")
    return


def callback1r(config, level):
    radar_ip = rospy.get_param('~radar_ip')
    exponent = config['exponent']
    clu_rm_thres = config['clutter_removal_threshold']
    scale_r = config['scale_r']
    scale_a = config['scale_a']

    clu_rm_thres = clu_rm_thres * 10**exponent
    print('clu_rm_thres:%.1f scale_r:%d scale_a:%a' % (clu_rm_thres,
                                                       scale_r,
                                                       scale_a))

    output_b = pack('<fff', clu_rm_thres,
				                 scale_r,
					             scale_a)
    output_b += (0).to_bytes(1012, byteorder='little') # 1024 whole package

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        # First radar
        client(radar_ip, output_b, s)
    return config


def callback2r(config, level):    
    radar1_ip = rospy.get_param('~radar1_ip')
    exponent_1 = config['exponent_1']
    clu_rm_thres_1 = config['clutter_removal_threshold_1']
    scale_r_1 = config['scale_r_1']
    scale_a_1 = config['scale_a_1']

    radar2_ip = rospy.get_param('~radar2_ip')
    exponent_2 = config['exponent_2']
    clu_rm_thres_2 = config['clutter_removal_threshold_2']
    scale_r_2 = config['scale_r_2']
    scale_a_2 = config['scale_a_2']

    clu_rm_thres_1 = clu_rm_thres_1 * 10**exponent_1
    clu_rm_thres_2 = clu_rm_thres_2 * 10**exponent_2
    print('clu_rm_thres:%.1f scale_r:%d scale_a:%a' % (clu_rm_thres_1,
                                                       scale_r_1,
                                                       scale_a_1))
    print('clu_rm_thres:%.1f scale_r:%d scale_a:%a' % (clu_rm_thres_2,
                                                       scale_r_2,
                                                       scale_a_2))

    output_1 = pack('<fff', clu_rm_thres_1,
				            scale_r_1,
					        scale_a_1)
    output_1 += (0).to_bytes(1012, byteorder='little') # 1024 whole package

    output_2 = pack('<fff', clu_rm_thres_2,
				            scale_r_2,
					        scale_a_2)
    output_2 += (0).to_bytes(1012, byteorder='little') # 1024 whole package

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        # First radar
        client(radar1_ip, output_1, s)
        # Second radar
        client(radar2_ip, output_2, s)
    return config

#%%

def callback1r2cl(config, level):
    
    radar_ip = rospy.get_param('~radar_ip')
    Threshold_d     = config['Threshold_d']
    Azimuth_sense_d = config['Azimuth_sense_d']
    RCS_filter_d    = config['RCS_filter_d']
    Threshold_s     = config['Threshold_s']
    Azimuth_sense_s = config['Azimuth_sense_s']
    RCS_filter_s    = config['RCS_filter_s']

    print('\nDynamic   Threshold: %i, Azimuth sense: %i, RCS filter: %i ' %
          (Threshold_d, Azimuth_sense_d, RCS_filter_d))
    print('Static    Threshold: %i Azimuth sense: %i RCS filter: %i\n' %
          (Threshold_s, Azimuth_sense_s, RCS_filter_s))

    output_b = pack('<BBHHHhhHHhhllll', 1, 0xC6, 36, Threshold_s,
                                                     Azimuth_sense_s,
                                                     RCS_filter_s, 0,
                                                     Threshold_d,
                                                     Azimuth_sense_d,
                                                     RCS_filter_d, 0,
                                                     0, 0, 0, 0)

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        # First radar
        client(radar_ip, output_b, s)
    return config

#%%

if __name__ == '__main__':
    rospy.init_node("send_params_to_radar", anonymous = False)
    cfg_type = rospy.get_param('~cfg_type')
    # print(cfg_type)
    if cfg_type == "one_radar":
        srv = Server(one_radarConfig, callback1r)
    elif cfg_type == "two_radars":
        srv = Server(two_radarsConfig, callback2r)
    elif cfg_type == "two_clouds_protocol":
        srv = Server(two_clouds_protocolConfig, callback1r2cl)
    rospy.spin()
