cmake_minimum_required(VERSION 3.0.2)
project(sensum_tuner)

add_compile_options(-std=c++14)

find_package(catkin REQUIRED COMPONENTS
  dynamic_reconfigure
  rospy
  std_msgs
)

generate_dynamic_reconfigure_options(
  cfg/one_radar.cfg
  cfg/two_radars.cfg
  cfg/two_clouds_protocol.cfg
)

catkin_package(
)

include_directories(
  ${catkin_INCLUDE_DIRS}
)

install(PROGRAMS
  src/send_default_params.py
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(DIRECTORY
  launch/
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/launch
)
